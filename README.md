# Mesosphere elevator

This project simulates a simple elevator controller. It manages up to 16 elevators. During the simulation each the
controller receives pickup request which indicate that a person wants to enter an elevator an move up/down. After
entering an elevator this person triggers an update request which specifies the floor this person wants to leave the
elevator.

To simplify the task I assume that all elevators have an unlimited capacity, i.e. that can contain an arbitrary amount
of people. Furthermore, all elevators are at the same place in the building and thus pickup requests aren't limited to
a certain group of elevators.

# Scheduling algorithm

The current algorithms aims for a better performance than only FCFS. To do so, each elevator will serve pickup requests
that are in their current direction of movement or they aren't moving at all. If there are multiple such elevators, the
closest one is selected. This way one elevator handles requests until it reached the highest target floor or the lowest
respectively that is on their way. The elevator controller makes sure that elevators get no pickup requests that are in
the opposite direction of their current movement. Nevertheless elevators support update requests that are in the
opposite direction of their current movement. Speaking from personal experience this happens way to often ;) 

# Future improvements

The simulation could be extended to cover more realistic scenarios. For example each elevator could be assigned with a
weight/person limit.

On the code point of view test coverage could be improved. Also having the controller handle all the state of all
elevators is error prone and could cause contention in case of many concurrent updates/request. Furthermore, I didn't
cover any cases of elevator failure. A simple strategy might be to just restart an elevator and reset all it's state. A
more advanced version could use akka persistence to store the current state (i.e. the current and target floor) and
recover the actor without loosing this information. Another improvement could be done to the modularity of this project.
It would be nice to support different scheduling algorithms that could be plugged in.

# Build instructions/Packaging

This application is written in scala 2.11.8 and requires java 8 to compile. As a build tool I used sbt 0.13.11. The
following sections show how to build/test/package this project.

## Build
```
sbt compile
```

## Test
```
sbt test
```

## Package
```
sbt universal:packageZipTarball
```
