name := "MesosphereElevator"

version := "1.0"

scalaVersion := "2.11.8"

projectDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.7" % Compile,
  "com.typesafe.akka" %% "akka-testkit" % "2.4.7" % Compile,
  "org.slf4j" % "slf4j-api" % "1.7.5" % Compile,
  "org.slf4j" % "slf4j-simple" % "1.7.5"  % Compile,
  "org.specs2" %% "specs2-core" % "3.8.3" % Test
)

scalacOptions += "-feature"

enablePlugins(JavaAppPackaging)
