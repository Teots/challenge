import akka.actor.ActorSystem
import control.SimpleElevatorControlSystem

import scala.concurrent.Await
import scala.concurrent.duration._

object Main extends App {
  implicit val system = ActorSystem("elevator-system")

  val controller = SimpleElevatorControlSystem(2)
  println(controller.status())
  controller.pickup(1, 1)
  println(controller.status())
  controller.step()
  controller.update(2, 1, 2)
  controller.step()
  println(controller.status())

  Thread.sleep(2000)

  Await.ready(system.terminate(), 5.seconds)
}
