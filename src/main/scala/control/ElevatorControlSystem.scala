package control

import actors.ElevatorController
import actors.Messages._
import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import org.slf4j.LoggerFactory

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * Layer of indirection to encapsulate the actor system
 */
trait ElevatorControlSystem {
  def status(): Seq[(Int, Int, Int)]

  def update(elevatorId: Int, floor: Int, targetFloor: Int)

  def pickup(floor: Int, direction: Int)

  def step()
}

/**
 * Simple implementation of the ElevatorControlSystem.
 */
class SimpleElevatorControlSystem private(private[control] val elevators: Int)(implicit ac: ActorSystem) extends ElevatorControlSystem {

  import ac.dispatcher
  import control.SimpleElevatorControlSystem._

  private implicit val timeout = Timeout(5.seconds)

  private val elevatorController = ac.actorOf(ElevatorController.props(elevators))

  override def status(): Seq[(Int, Int, Int)] = {
    val statusFut = (elevatorController ? StatusRequest).mapTo[StatusResponse]

    statusFut onComplete {
      case Success(s) => logger.debug(s"Successfully got status: $s")
      case Failure(e) => logger.error(s"Could not retrieve status", e)
    }

    Await.result(statusFut, timeout.duration).elevatorStatus.map(_.toTuple)
  }

  override def update(elevatorId: Int, floor: Int, targetFloor: Int) =
    elevatorController ! Update(elevatorId, floor, targetFloor)

  override def pickup(floor: Int, direction: Int): Unit = elevatorController ! Pickup(floor, direction)

  override def step(): Unit = elevatorController ! Step
}

object SimpleElevatorControlSystem {
  private val logger = LoggerFactory.getLogger(classOf[SimpleElevatorControlSystem])

  private val ElevatorLimit = 16

  /**
   * Creates a new ElevatorControlSystem, but checks the number of elevators first.
   */
  def apply(elevators: Int)(implicit ac: ActorSystem) = {
    val numberElevators = if (elevators > 0 && elevators <= ElevatorLimit) {
      elevators
    } else {
      logger.warn(s"The number of elevators ($elevators) wasn't in the specified range [1, 16]")

      if (elevators <= 0) math.max(1, elevators)
      else math.min(16, elevators)
    }

    new SimpleElevatorControlSystem(numberElevators)
  }
}
