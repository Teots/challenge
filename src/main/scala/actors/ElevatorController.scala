package actors

import actors.Messages._
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * This actor manages all existing elevators. It's also responsible for scheduling the incoming pickup requests.
 */
class ElevatorController(elevators: Int) extends Actor with ActorLogging {
  private implicit val timeout = Timeout(5.seconds)

  private val elevatorMap: mutable.HashMap[Int, ElevatorStatus] = initElevators(elevators)

  private val queuedRequests = mutable.ListBuffer.empty[Pickup]

  override def receive: Receive = {
    case StatusRequest =>
      sender ! StatusResponse(elevatorMap.values.map(_.status).toSeq)

    case p@Pickup(floor, direction) => {
      // Find an appropriate elevator or store the request
      val elevatorOpt = findElevatorForPickup(p)

      elevatorOpt.fold[Unit] {
        log.debug(s"Queue pickup request for floor $floor [$direction]")

        queuedRequests += p
      } { elevatorId =>
        log.debug(s"Forward pickup request for floor $floor [$direction] to elevator $elevatorId")

        sendStatusChangingRequest(elevatorId, p)
      }
    }

    case u@Update(elevatorId, floor, targetFloor) => sendStatusChangingRequest(elevatorId, u)

    case Step => {
      // Each elevator performs a step now
      elevatorMap foreach { case (elevatorId, _) =>
        sendStatusChangingRequest(elevatorId, Step)
      }

      // Check whether any of the remaining requests can be assigned now
      val queueIterator = queuedRequests.iterator

      while(queueIterator.hasNext) {
        val pickup = queueIterator.next()
        val elevatorOpt = findElevatorForPickup(pickup)

        elevatorOpt.foreach { elevatorId =>
          log.debug(s"Forward stored pickup request for floor ${pickup.floor} [${pickup.direction}] to elevator $elevatorId")

          sendStatusChangingRequest(elevatorId, pickup)
          queuedRequests -= pickup
        }
      }
    }

    case _ => log.error("Received unknown message")
  }

  private def sendStatusChangingRequest(elevatorId: Int, request: Any) = {
    val elevatorRef = elevatorMap(elevatorId).elevator
    val updatedStatus = Await.result((elevatorRef ? request).mapTo[Status], timeout.duration)

    elevatorMap.put(elevatorId, ElevatorStatus(elevatorRef, updatedStatus))
  }

  private def initElevators(elevators: Int): mutable.HashMap[Int, ElevatorStatus] = {
    (
      for (i <- 1 to elevators) yield {
        i -> ElevatorStatus(context.actorOf(Elevator.props(i)), Status(i, 0, 0))
      }
      )(collection.breakOut)
  }

  /**
   * This method contains the main part of the scheduling algorithm.
   *
   * @param pickup The pickup request.
   *
   * @return An elevator that can serve the pickup request. In case no suited elevator exists, None is returned.
   */
  private def findElevatorForPickup(pickup: Pickup): Option[Int] = {
    val statuses = elevatorMap.values.map(_.status).toSeq

    // Get the elevators going in the same direction as the pickup request to satisfy it on the way
    val pickupElevators = statuses filter { case Status(id, floor, targetFloor) =>
      if (pickup.direction > 0) {
        // Is the elevator going up as well or has no task at the moment?
        if (floor <= targetFloor) {
          // Is a pickup on the way or later possible
          floor <= pickup.floor
        } else {
          // Not the same direction
          false
        }
      } else {
        // Is the elevator going up as well or has no task at the moment?
        if (floor >= targetFloor) {
          // Is a pickup on the way or later possible
          floor >= pickup.floor
        } else {
          // Not the same direction
          false
        }
      }
    }

    // Get the closest elevator for pickup
    pickupElevators sortBy { case Status(id, floor, targetFloor) =>
      math.abs(floor - pickup.floor)
    } map(_.elevatorId) headOption
  }
}

object ElevatorController {
  def props(elevators: Int): Props = Props(new ElevatorController(elevators))
}

final case class ElevatorStatus(elevator: ActorRef, status: Status)
