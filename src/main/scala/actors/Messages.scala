package actors

/**
 * Hold all messages that exist in this actor system.
 */
object Messages {
  final case object StatusRequest

  final case class StatusResponse(elevatorStatus: Seq[Status])

  final case class Status(elevatorId: Int, floor: Int, targetFloor: Int) {
    def toTuple = (elevatorId, floor, targetFloor)
  }

  final case class Update(elevatorId: Int, floor: Int, targetFloor: Int)

  final case class Pickup(floor: Int, direction: Int)

  final case object Step
}
