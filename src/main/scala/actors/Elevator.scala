package actors

import actors.Messages.{Status, Pickup, Update, Step}
import akka.actor.{Props, ActorLogging, Actor}
import akka.actor.Actor.Receive

import scala.collection.mutable

/**
 * This actor represents an elevator.
 *
 * @param id The elevator id
 */
class Elevator(id: Int) extends Actor with ActorLogging {
  private var floor = 0
  private var targetFloor = 0

  private[actors] val floorsToStopAt = mutable.HashSet.empty[Int]

  override def receive: Receive = {
    case Pickup(pickupFloor, _) => {
      if (targetFloor > floor) targetFloor = math.max(targetFloor, pickupFloor)
      else if (targetFloor < floor) targetFloor = math.min(targetFloor, pickupFloor)
      else targetFloor = pickupFloor

      log.debug(s"Elevator $id got a pickup request for floor $pickupFloor - target floor is now $targetFloor")

      sender ! Status(id, floor, targetFloor)
    }

    case Update(_, _, tFloor) => {
      // Update the list of floors where to stop, if you aren't here already
      if (floor != tFloor) floorsToStopAt += tFloor

      /*
       * Update the target floor, if this elevator didn't get a request for the wrong direction. Later would be handled
       * by the doStep() method.
       */
      // Going up
      if (targetFloor > floor) {
        if (tFloor > floor) targetFloor = math.max(targetFloor, tFloor)
      }
      // Going down
      else if (targetFloor < floor) {
        if (tFloor < floor) targetFloor = math.min(targetFloor, tFloor)
      }
      // Start a new movement
      else targetFloor = tFloor

      log.debug(s"Elevator $id got an update request for floor $tFloor - target floor is now $targetFloor")

      sender ! Status(id, floor, targetFloor)
    }

    case Step => {
      doStep()

      sender ! Status(id, floor, targetFloor)
    }

    case _ => log.error("Received unknown message")
  }

  private def doStep() = {
    log.debug(s"Elevator $id performs a step")

    // Move the elevator and remove the new floor from the target list
    if (targetFloor > floor) floor += 1
    else if (targetFloor < floor) floor -= 1

    floorsToStopAt -= floor

    // Check whether the target floor was reached and there are open request
    if (floor == targetFloor && floorsToStopAt.nonEmpty) {
      // Check direction
      if (floorsToStopAt.max < floor) {
        // Go down now
        targetFloor = floorsToStopAt.min
      } else {
        // Go up now
        targetFloor = floorsToStopAt.max
      }
    }
  }
}

object Elevator {
  def props(id: Int) = Props(new Elevator(id))
}
