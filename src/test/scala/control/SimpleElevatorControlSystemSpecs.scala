package control

import akka.actor.ActorSystem
import org.specs2.mutable.Specification

class SimpleElevatorControlSystemSpecs extends Specification {
  implicit val ac = ActorSystem("elevator-test-system")

  "SimpleElevatorControlSystem creation" >> {
    "Less that 1 elevator" >> {
      val elevatorSystem = SimpleElevatorControlSystem(-1)

      elevatorSystem.elevators must beEqualTo(1)
    }

    "More that 16 elevators" >> {
      val elevatorSystem = SimpleElevatorControlSystem(17)

      elevatorSystem.elevators must beEqualTo(16)
    }

    "0 elevators" >> {
      val elevatorSystem = SimpleElevatorControlSystem(0)

      elevatorSystem.elevators must beEqualTo(1)
    }

    "1 elevator" >> {
      val elevatorSystem = SimpleElevatorControlSystem(1)

      elevatorSystem.elevators must beEqualTo(1)
    }

    "16 elevators" >> {
      val elevatorSystem = SimpleElevatorControlSystem(16)

      elevatorSystem.elevators must beEqualTo(16)
    }

    "10 elevator2" >> {
      val elevatorSystem = SimpleElevatorControlSystem(10)

      elevatorSystem.elevators must beEqualTo(10)
    }
  }
}

