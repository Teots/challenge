package actors

import actors.Messages.{Update, Step, Pickup, Status}
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import org.specs2.mutable._

import scala.concurrent.duration._
import scala.util.Success

abstract class AkkaTestkitSpecs2Support extends TestKit(ActorSystem("elevator-test-system")) with After {
  // Shut down the actor system after all tests have run
  def after = system.terminate()
}

class ElevatorSpecs extends Specification {
  sequential // forces all tests to be run sequentially

  implicit val timeout = Timeout(1.second)

  "Elevator request handling" >> {
    "Update target floor on pickup request" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef(Elevator.props(1))

        val future = elevator ? Pickup(2, -1)
        val Success(result: Status) = future.value.get
        result.targetFloor must beEqualTo(2)
      }
    }

    "Move floor on after step" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef(Elevator.props(1))

        elevator ! Pickup(2, -1)
        val future = elevator ? Step

        val Success(result: Status) = future.value.get
        result.targetFloor must beEqualTo(2)
        result.floor must beEqualTo(1)
      }
    }

    "Get update request while not moving" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef[Elevator](Elevator.props(1))

        // Pickup person
        elevator ! Pickup(2, -1)
        elevator ! Step
        elevator ! Step

        // Update request
        val future = elevator ? Update(1, 2, 0)

        val Success(result: Status) = future.value.get
        result.targetFloor must beEqualTo(0)
        result.floor must beEqualTo(2)
      }
    }

    "Get update request in direction of moving" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef[Elevator](Elevator.props(1))

        // Pickup person
        elevator ! Pickup(2, 1)
        elevator ! Step
        elevator ! Step
        elevator ! Update(1, 2, 0)
        elevator ! Step

        val future = elevator ? Step

        val Success(result: Status) = future.value.get
        elevator.underlyingActor.floorsToStopAt must beEmpty
        result.targetFloor must beEqualTo(0)
        result.floor must beEqualTo(0)
      }
    }

    "Get update request in opposite direction of moving - ignored" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef[Elevator](Elevator.props(1))

        // Pickup person
        elevator ! Pickup(2, 1)
        elevator ! Step
        elevator ! Pickup(10, 1)
        elevator ! Step
        elevator ! Update(1, 2, 0)
        elevator ! Step

        val future = elevator ? Step

        val Success(result: Status) = future.value.get
        elevator.underlyingActor.floorsToStopAt must containAllOf(Seq(0))
        result.targetFloor must beEqualTo(10)
        result.floor must beEqualTo(4)
      }
    }

    "Get update request in opposite direction of moving - accepted after finishing current movement" >> new AkkaTestkitSpecs2Support {
      within(1.second) {
        val elevator = TestActorRef[Elevator](Elevator.props(1))

        // Pickup person
        elevator ! Pickup(2, 1)
        elevator ! Step
        elevator ! Pickup(10, 1)
        elevator ! Step
        elevator ! Step
        elevator ! Step
        elevator ! Update(1, 2, 3)
        elevator ! Step
        elevator ! Step
        elevator ! Step
        elevator ! Step
        elevator ! Step

        val future = elevator ? Step

        val Success(result: Status) = future.value.get
        elevator.underlyingActor.floorsToStopAt must containAllOf(Seq(3))
        result.targetFloor must beEqualTo(3)
        result.floor must beEqualTo(10)
      }
    }
  }
}
